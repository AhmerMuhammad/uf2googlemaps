package com.example.mapassignment.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.mapassignment.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map;

import me.shaohui.advancedluban.Luban;
import me.shaohui.advancedluban.OnCompressListener;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }


    EditText edCountry,edStreetAddress,edCity;
    private Button sendDatabtn;
    String encodedImage="";

    FirebaseDatabase firebaseDatabase;


  public static   DatabaseReference databaseReference,databaseReference1;
    public static List<String> list = new ArrayList<>();

    PlacesInfo placesInfo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = new ViewModelProvider(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    ImageView imgUpload;
    ListView listview;
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);


        edStreetAddress = root.findViewById(R.id.edStreetAddress);
        edCity = root.findViewById(R.id.edCity);
        edCountry = root.findViewById(R.id.edCountry);
        imgUpload = root.findViewById(R.id.imgUpload);
        listview = root.findViewById(R.id.listview);

        imgUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkCameraPermission()) {

                    if (getContext() != null) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    chooseOrTakePictureFor();
                }

            }
        });


        firebaseDatabase = FirebaseDatabase.getInstance("https://mapa-533c2-default-rtdb.firebaseio.com/");// below line is used to get reference for our database.
        databaseReference1 = firebaseDatabase.getReference("PlacesInfo");


        databaseReference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Map<String, String> map = (Map<String, String>) snapshot.getValue();
                list.add(map.get("streetAddress") + ", " + map.get("cityName") + ", " + map.get("countryName"));
                Log.d("snapshot ", map.get("countryName"));

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

        placesInfo = new PlacesInfo();

        sendDatabtn = root.findViewById(R.id.idBtnSendData);

        sendDatabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference = firebaseDatabase.getReference("PlacesInfo").child(list.size()+"");

                String StreetAddress = edStreetAddress.getText().toString();
                String City = edCity.getText().toString();
                String Country = edCountry.getText().toString();


                if (TextUtils.isEmpty(StreetAddress) && TextUtils.isEmpty(City) && TextUtils.isEmpty(Country)) {

                    Toast.makeText(getActivity(), "Rellena todos los campos", Toast.LENGTH_SHORT).show();
                }else if (encodedImage.length()<100) {

                    Toast.makeText(getActivity(), "Selecciona una Imagen", Toast.LENGTH_SHORT).show();
                } else {

                    addDatatoFirebase(StreetAddress, City, Country,encodedImage);
                }
            }
        });

        return root;
    }


    private void addDatatoFirebase(String street, String city, String country,String encodedImage) {

        placesInfo.setStreetAddress(street);
        placesInfo.setCityName(city);
        placesInfo.setCountryName(country);
        placesInfo.setImageString(encodedImage);

        list.clear();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                databaseReference.setValue(placesInfo);

                Toast.makeText(getActivity(), "Datos Añadidos ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                Toast.makeText(getActivity(), "Error al Añadir los datos " + error, Toast.LENGTH_SHORT).show();
            }
        });


        databaseReference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Map<String, String> map = (Map<String, String>) snapshot.getValue();
                list.add(map.get("streetAddress") + ", " + map.get("cityName") + ", " + map.get("countryName"));
                Log.d("snapshot ", map.get("countryName")+list.size());

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    public void chooseOrTakePictureFor() {
        boolean hasPermissions = EasyPermissions.hasPermissions(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasPermissions) {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(getActivity(),this);
        } else {
            EasyPermissions.requestPermissions(this,
                    "Permissions required for adding photos", 1,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();
            String path = FileUtils.getPath(getContext(), resultUri);
            compressFile(path);
            Log.e("path",path);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private void compressFile(final String path) {
        Luban.compress(getActivity(), new File(path))
                .setMaxSize(50)
                .setMaxHeight(320)
                .setMaxWidth(200)
                .putGear(Luban.CUSTOM_GEAR)
                .launch(new OnCompressListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onSuccess(File file) {
                        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                        Picasso.with(getActivity()).load(file).into(imgUpload);
                        Log.e("encodedImage",encodedImage);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }



    
}